/*
   AudioSynthSinePartial

   AudioSynthSinePartial is a C++ class designed for Teensy Microcontrollers with an FPU
   on board (>=3.5). It generates the following waveform types based on addition of sin/cos
   waves (iFFT):
   - square/pulse
   - triangle
   - sawtooth (up/down)

MIT License

Copyright (c)2023 Holger Wirtz <wirtz@parasitstudio.de>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

#pragma once

#if !defined(ARDUINO_TEENSY35) && !defined(ARDUINO_TEENSY36) && !defined(ARDUINO_TEENSY40) && !defined(ARDUINO_TEENSY40MM) && !defined(ARDUINO_TEENSY41)
#error This class works only for Teensy MCUs with FPU (floating point unit)!
#endif

#include <math.h>
#include <limits.h>
#include "Arduino.h"
#include "AudioStream.h"

#define MIN_PARTIALS 1
#define MAX_PARTIALS 255
#define DEFAULT_RESOLUTION 100
#define DUTY_CYCLE_MIN 0.1
#define DUTY_CYCLE_MAX 1.0
#define DAMPING_FACTOR M_SQRT1_2

#define HALF_PERIOD M_PI
#define PERIOD 2.0*HALF_PERIOD
#define USE_CMSIS

#if defined(USE_CMSIS)
#include "arm_math.h"
#endif

class AudioSynthSinePartial : public AudioStream
{
public:
	AudioSynthSinePartial(uint8_t p);
	~AudioSynthSinePartial();
	void frequency(float freq);
	void amplitude(float n);
	void partials(uint8_t p);
	uint16_t resolution(uint16_t r);
	uint16_t get_resolution(void);
	void show_partials(void);
	void update(void);
	void bypass(bool b);
	float midi_note_to_frequency(uint8_t note);

protected:
	void _calculate(void);
	virtual float _f(float x)=0;

	uint8_t _partials;
	float _frequency;
	float _amplitude;
	float* _a;
	float* _b;
	float _phase_increment;
	volatile float _phase_accumulator;
	bool _bypass;
	float _resolution;
	bool _rising;
};

class AudioSynthSineSquare : public AudioSynthSinePartial
{
public:
	AudioSynthSineSquare(uint8_t p);
	~AudioSynthSineSquare();
	void duty_cycle(float dc);

protected:
        float _f(float x);

	float _duty_cycle;
};

class AudioSynthSineTriangle : public AudioSynthSinePartial
{
public:
	AudioSynthSineTriangle(uint8_t p);
	~AudioSynthSineTriangle();

protected:
        float _f(float x);
};

class AudioSynthSineSawtooth : public AudioSynthSinePartial
{
public:
	AudioSynthSineSawtooth(uint8_t p);
	AudioSynthSineSawtooth(uint8_t p, bool r);
	~AudioSynthSineSawtooth();
	void rising(bool r);
	void falling(bool r);
protected:
        float _f(float x);
};

#ifndef _FLOAT2INT
#define _FLOAT2INT
inline int float2int(float f)
{
	return(f*32768.0+0.5);
}
#endif

#ifndef _MAPFLOAT
#define _MAPFLOAT
inline float mapfloat(float val, float in_min, float in_max, float out_min, float out_max) {
  return (val - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
#endif


