/*
   AudioSynthSinePartialFast

   AudioSynthSinePartialFast is a C++ class designed for Teensy Microcontrollers with an FPU
   on board (>=3.5). It generates the following waveform types based on addition of sin/cos
   waves (iFFT):
   - square/pulse
   - triangle
   - sawtooth (up/down)

   The mathematics behind this class is decribed in
   https://lpsa.swarthmore.edu/Fourier/Series/ExFS.html
   
MIT License

Copyright (c)2023 Holger Wirtz <wirtz@parasitstudio.de>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

#include "synth_sine_partial_fast.h"

/***********************************************************************
 * Base class
 ***********************************************************************/

AudioSynthSinePartialFast::AudioSynthSinePartialFast(uint8_t p=MIN_PARTIALS) : AudioStream(0, NULL)
{
        _a=NULL;
        _partials=p;
        _frequency=0.0;
        _amplitude=0.0;
	_phase_increment=0.9;
	_phase_accumulator=0.0;
	_rising=false;
	_bypass=false;
}

AudioSynthSinePartialFast::~AudioSynthSinePartialFast()
{
        delete(_a);
}

void AudioSynthSinePartialFast::frequency(float32_t freq)
{
        if(freq < 0.0)
                _frequency=0.0;
        if(freq > (AUDIO_SAMPLE_RATE_EXACT/2.0))
                _frequency=(AUDIO_SAMPLE_RATE_EXACT/2.0);
        else
                _frequency=freq;

        _phase_increment=(2.0*M_PI*_frequency)/AUDIO_SAMPLE_RATE_EXACT;
        _phase_accumulator=0.0;

        _calculate();
}

void AudioSynthSinePartialFast::amplitude(float32_t n)
{
        if(_amplitude < 0.0)
                _amplitude=0.0;
        else
                _amplitude=n;
        _calculate();
}

void AudioSynthSinePartialFast::partials(uint8_t p)
{
        if(p < MIN_PARTIALS)
                _partials=MIN_PARTIALS;
        else if (p > MAX_PARTIALS)
                _partials=MAX_PARTIALS;
        else
                _partials=p;

	_calculate();
}

void AudioSynthSinePartialFast::show_partials(void)
{
	if(_a)
	{
        	for(uint8_t i=0;i<_partials;i++)
                	Serial.printf("a[%d] = %5.3f\n", i, _a[i]);
	}
}

void AudioSynthSinePartialFast::update(void)
{
	float32_t block_f32[AUDIO_BLOCK_SAMPLES];
	float32_t ph=_phase_accumulator;

	if (_bypass || _frequency == 0.0 || _amplitude == 0.0 || _a == NULL)
		return;

	audio_block_t *out_block = allocate();

   	if (!out_block)
		return;

	for(uint8_t n=0;n<AUDIO_BLOCK_SAMPLES;n++)
	{
		block_f32[n]=_a[0];
		for(uint8_t p=1; p<_partials; p++)
		{
			if(_a[p]!=0.0)
                        	block_f32[n]+=_a[p]*_internal_update_function(p*ph);
		}
#if !defined(USE_CMSIS)
                if(_rising)
                        out_block->data[n] = -1.0 *  _amplitude * float2int(block_f32[n]);
                else
                        out_block->data[n] = _amplitude * float2int(block_f32[n]);
#endif

		ph+=_phase_increment;
	}

#if defined(USE_CMSIS)
        if(_rising)
                arm_scale_f32(block_f32,(-1.0),block_f32,AUDIO_BLOCK_SAMPLES);

	arm_float_to_q15(block_f32, out_block->data, AUDIO_BLOCK_SAMPLES);
#endif

	transmit(out_block);
	release(out_block);

	_phase_accumulator=fmodf(ph,2.0*M_PI);
}

float AudioSynthSinePartialFast::midi_note_to_frequency(uint8_t note)
{
        const float a = 440.0;
        return (a / 32) * pow(2, ((note - 9) / 12.0));
}

float AudioSynthSinePartialFast::_internal_update_function(float phase)
{
#ifdef USE_CMSIS
	return(arm_cos_f32(phase));
#else
	return(cos(phase));
#endif
}

/***********************************************************************
 * Square waveform class
 ***********************************************************************/

AudioSynthSineSquareFast::AudioSynthSineSquareFast(uint8_t p) : AudioSynthSinePartialFast(p)
{
	_duty_cycle = 1.0;
	_calculate();
}

AudioSynthSineSquareFast::~AudioSynthSineSquareFast() { }

void AudioSynthSineSquareFast::duty_cycle(float32_t dc)
{
        if(dc < 0.0)
                _duty_cycle=0.0;
        if(dc > 1.0)
                _duty_cycle=1.0;
        else
                _duty_cycle=dc;
        _calculate();
}

void AudioSynthSineSquareFast::_calculate(void)
{
        float* a_old = _a;
        float* a = new float[_partials]();

	if(_duty_cycle!=1.0)
		a[0]=(_duty_cycle/2.0)-M_1_PI;

	for(uint8_t p=1; p<_partials;p++)
	{
		if(_duty_cycle==1.0)
			if(p%2==0)
				a[p]=0.0;
			else
				a[p]=2*(_amplitude/(p*M_PI))*pow(-1.0,(p-1)/2.0); // special case
		else
#ifdef USE_CMSIS
			a[p]=2*(_amplitude/(p*M_PI))*arm_sin_f32(_duty_cycle*p*M_PI_2);
#else
			a[p]=2*(_amplitude/(p*M_PI))*sin(_duty_cycle*p*M_PI_2);
#endif
	}

        __disable_irq();
        _a=a;
        __enable_irq();

        delete(a_old);
}

/***********************************************************************
 * Triangle waveform class
 ***********************************************************************/

AudioSynthSineTriangleFast::AudioSynthSineTriangleFast(uint8_t p) : AudioSynthSinePartialFast(p)
{
	_calculate();
}

AudioSynthSineTriangleFast::~AudioSynthSineTriangleFast() { }

void AudioSynthSineTriangleFast::_calculate(void)
{
        float* a_old = _a;
        float* a = new float[_partials]();

	a[0]=0.0;
	for(uint8_t p=1; p<_partials;p++)
		a[p]=4.0*_amplitude*((1.0-pow(-1.0,p))/(pow(M_PI,2.0)*pow(p,2.0)));

        __disable_irq();
        _a=a;
        __enable_irq();

        delete(a_old);
}

/***********************************************************************
 * Sawtooth waveform class
 ***********************************************************************/

AudioSynthSineSawtoothFast::AudioSynthSineSawtoothFast(uint8_t p) : AudioSynthSinePartialFast(p)
{
	_rising=true;
	_calculate();
}

AudioSynthSineSawtoothFast::AudioSynthSineSawtoothFast(uint8_t p, bool r) : AudioSynthSinePartialFast(p)
{
	rising(r);
	_calculate();
}

AudioSynthSineSawtoothFast::~AudioSynthSineSawtoothFast() { }

void AudioSynthSineSawtoothFast::_calculate(void)
{
        float* a_old = _a;
        float* a = new float[_partials]();

	for(uint8_t p=1; p<=_partials;p++)
			a[p-1]=(2.0*_amplitude)/(M_PI*p)*pow(-1.0,p)*_rising;
	a[0]+=M_2_PI;

        __disable_irq();
        _a=a;
        __enable_irq();

        delete(a_old);
}

float AudioSynthSineSawtoothFast::_internal_update_function(float phase)
{
#ifdef USE_CMSIS
	return(arm_sin_f32(phase));
#else
	return(sin(phase));
#endif
}

void AudioSynthSineSawtoothFast::rising(bool r)
{
	_rising=r;
}

void AudioSynthSineSawtoothFast::falling(bool r)
{
	rising(!r);
}
