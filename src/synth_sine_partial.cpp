/*
   AudioSynthSinePartial

   AudioSynthSinePartial is a C++ class designed for Teensy Microcontrollers with an FPU
   on board (>=3.5). It generates the following waveform types based on addition of sin/cos
   waves (iFFT):
   - square/pulse
   - triangle
   - sawtooth (up/down)

MIT License

Copyright (c)2023 Holger Wirtz <wirtz@parasitstudio.de>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

#include "synth_sine_partial.h"

/***********************************************************************
 * Base class
 ***********************************************************************/

AudioSynthSinePartial::AudioSynthSinePartial(uint8_t p=MIN_PARTIALS) : AudioStream(0, NULL)
{
        _a=NULL;
        _b=NULL;
        _partials=p;
        _frequency=0.0;
        _amplitude=0.0;
	_phase_increment=0.9;
	_phase_accumulator=0.0;
	_bypass=false;
	_resolution=PERIOD/float(DEFAULT_RESOLUTION);
	_rising=false;
}

AudioSynthSinePartial::~AudioSynthSinePartial()
{
        delete(_a);
        delete(_b);
}

void AudioSynthSinePartial::frequency(float freq)
{
        if(freq < 0.0)
                _frequency=0.0;
        if(freq > (AUDIO_SAMPLE_RATE_EXACT/2.0))
                _frequency=(AUDIO_SAMPLE_RATE_EXACT/2.0);
        else
                _frequency=freq;

        _phase_increment=(TWO_PI*_frequency)/AUDIO_SAMPLE_RATE_EXACT;
        _phase_accumulator=0.0;
}

void AudioSynthSinePartial::amplitude(float n)
{
        if(_amplitude < 0.0)
                _amplitude=0.0;
        else
                _amplitude=DAMPING_FACTOR * n;
}

void AudioSynthSinePartial::partials(uint8_t p)
{
        if(p < MIN_PARTIALS)
                _partials=MIN_PARTIALS;
        else if (p > MAX_PARTIALS)
                _partials=MAX_PARTIALS;
        else
                _partials=p;

	if(get_resolution() < (_partials * 2 + 1))
		resolution(_partials);

	_calculate();
}

void AudioSynthSinePartial::show_partials(void)
{
	Serial.printf("Partials: %d, Resolution: %d\n",_partials,get_resolution());

	if(_a && _b)
	{
        	for(uint8_t i=0;i<_partials;i++)
                	Serial.printf("a[%d] = %5.3f   b[%d] = %5.3f\n", i, _a[i], i, _b[i]);

               	Serial.printf("f(x)=%5.5f", _a[0]);
        	for(uint8_t i=1;i<_partials;i++)
                	Serial.printf("%+5.5f*cos(%f*x*%d)%+5.5f*sin(%f*x*%d)", _a[i], TWO_PI, i, _b[i], TWO_PI, i);
		Serial.printf("\n");
	}
}

void AudioSynthSinePartial::update(void)
{
	float block_f32[AUDIO_BLOCK_SAMPLES];
	float ph=_phase_accumulator;

	if (_bypass || _frequency == 0.0 || _amplitude == 0.0 || !_a  || !_b )
		return;

	audio_block_t *out_block = allocate();

   	if (!out_block)
		return;

	for(uint8_t n=0;n<AUDIO_BLOCK_SAMPLES;n++)
	{
		block_f32[n] = _a[0];
		for(uint8_t p=1; p<_partials; p++)
		{
#if defined(USE_CMSIS)
				block_f32[n] += _a[p] * arm_cos_f32(p*ph) + _b[p] * arm_sin_f32(p*ph);
#else
				block_f32[n] += _a[p] * cos(p*ph) + _b[p] * sin(p*ph);
#endif
		}

#if !defined(USE_CMSIS)
		if(!_rising)
			out_block->data[n] = -1.0 *  _amplitude * float2int(block_f32[n]);
		else
			out_block->data[n] = _amplitude * float2int(block_f32[n]);
#endif
		ph+=_phase_increment;
	}

#if defined(USE_CMSIS)
	if(!_rising)
		arm_scale_f32(block_f32,(-1.0)*_amplitude,block_f32,AUDIO_BLOCK_SAMPLES);
	else
		arm_scale_f32(block_f32,_amplitude,block_f32,AUDIO_BLOCK_SAMPLES);
	arm_float_to_q15(block_f32,out_block->data,AUDIO_BLOCK_SAMPLES);
#endif

	transmit(out_block);
	release(out_block);

	_phase_accumulator=fmodf(ph,TWO_PI);
}

void AudioSynthSinePartial::bypass(bool b)
{
	_bypass=b;
}

uint16_t AudioSynthSinePartial::resolution(uint16_t r)
{
	if(r < 2 || r < (_partials * 2 + 1))
		r=_partials * 2 + 1;

	_resolution=PERIOD/float(r);

	_calculate();

	return(_resolution);
}

uint16_t AudioSynthSinePartial::get_resolution(void)
{
	return((1.0/_resolution*PERIOD)+0.5);
}

float AudioSynthSinePartial::midi_note_to_frequency(uint8_t note)
{
	const float a = 440.0;
	return (a / 32) * pow(2, ((note - 9) / 12.0));
}

void AudioSynthSinePartial::_calculate(void)
{
	float* a_old = _a;
	float* b_old = _b;
	float* a = new float[_partials]();
	float* b = new float[_partials]();

	for (float x = -1.0*M_PI; x <= M_PI; x += _resolution)
		a[0] += _f(x) * _resolution;
	a[0]/=TWO_PI;

	for(uint8_t p=1; p<_partials;p++)
	{
		for (float x = -1.0*M_PI; x <= M_PI; x += _resolution) {
#if defined(USE_CMCSIS)
			a[p] += _f(x) * arm_cos_f32(p*x) * _resolution;
       			b[p] += _f(x) * arm_sin_f32(p*x) * _resolution;
#else
			a[p] += _f(x) * cos(p*x) * _resolution;
       			b[p] += _f(x) * sin(p*x) * _resolution;
#endif
    		}
		a[p]/=M_PI;
		b[p]/=M_PI;
	}

	__disable_irq();
	_a=a;
	_b=b;
	__enable_irq();

        delete(a_old);
        delete(b_old);
}

/***********************************************************************
 * Square waveform class
 ***********************************************************************/

AudioSynthSineSquare::AudioSynthSineSquare(uint8_t p) : AudioSynthSinePartial(p)
{
	_duty_cycle = 1.0;
}

AudioSynthSineSquare::~AudioSynthSineSquare() { }

void AudioSynthSineSquare::duty_cycle(float dc)
{
        if(dc < DUTY_CYCLE_MIN)
                _duty_cycle=DUTY_CYCLE_MIN;
        if(dc > DUTY_CYCLE_MAX)
                _duty_cycle=DUTY_CYCLE_MAX;
        else
                _duty_cycle=dc;
        _calculate();
}

float AudioSynthSineSquare::_f(float x)
{
	x = fmod(x, M_PI);
	if (x < M_PI - (_duty_cycle * M_PI))
		return(1.0);
	else
        	return(-1.0);
}

/***********************************************************************
 * Triangle waveform class
 ***********************************************************************/

AudioSynthSineTriangle::AudioSynthSineTriangle(uint8_t p) : AudioSynthSinePartial(p) { }

AudioSynthSineTriangle::~AudioSynthSineTriangle() { }

float AudioSynthSineTriangle::_f(float x)
{
    float mod = fmod(fabs(x), 2 * M_PI);
    if (mod <= M_PI) {
        return(2 * mod / M_PI) - 1;
    } else {
        return(-((2 * (mod - M_PI)) / M_PI) + 1);
    }
}

/***********************************************************************
 * Sawtooth waveform class
 ***********************************************************************/

AudioSynthSineSawtooth::AudioSynthSineSawtooth(uint8_t p) : AudioSynthSinePartial(p)
{
	_rising=true;
}

AudioSynthSineSawtooth::AudioSynthSineSawtooth(uint8_t p, bool r) : AudioSynthSinePartial(p)
{
	rising(r);
}

AudioSynthSineSawtooth::~AudioSynthSineSawtooth() { }

float AudioSynthSineSawtooth::_f(float x)
{
    float slope = 1.0 / (HALF_PERIOD);

    x = fmod(x, PERIOD);
    if (x < HALF_PERIOD)
        return slope*x;
    else
        return 1.0 - slope*(x - HALF_PERIOD);
}

void AudioSynthSineSawtooth::rising(bool r)
{
	_rising=r;
}

void AudioSynthSineSawtooth::falling(bool r)
{
	rising(!r);
}

