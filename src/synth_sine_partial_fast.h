/*
   AudioSynthSinePartialFast

   AudioSynthSinePartialFast is a C++ class designed for Teensy Microcontrollers with an FPU
   on board (>=3.5). It generates the following waveform types based on addition of sin/cos
   waves (iFFT):
   - square/pulse
   - triangle
   - sawtooth (up/down)

   The mathematics behind this class is decribed in
   https://lpsa.swarthmore.edu/Fourier/Series/ExFS.html
   
MIT License

Copyright (c)2023 Holger Wirtz <wirtz@parasitstudio.de>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

*/

#pragma once

#if !defined(ARDUINO_TEENSY35) && !defined(ARDUINO_TEENSY36) && !defined(ARDUINO_TEENSY40) && !defined(ARDUINO_TEENSY40MM) && !defined(ARDUINO_TEENSY41)
#error This class works only for Teensy MCUs with FPU (floating point unit)!
#endif

#include <math.h>
#include <limits.h>
#include "Arduino.h"
#include "AudioStream.h"

#define MIN_PARTIALS 1
#define MAX_PARTIALS 255
#define DUTY_CYCLE_MIN 0.1
#define DUTY_CYCLE_MAX 1.0
#define USE_CMSIS

#if defined(USE_CMSIS)
#include "arm_math.h"
#endif

class AudioSynthSinePartialFast : public AudioStream
{
public:
	AudioSynthSinePartialFast(uint8_t p);
	~AudioSynthSinePartialFast();
	void frequency(float freq);
	void amplitude(float n);
	void partials(uint8_t p);
	void show_partials(void);
        void update(void);
        void bypass(bool b);
        float midi_note_to_frequency(uint8_t note);

protected:
	virtual void _calculate(void)=0;
	virtual float _internal_update_function(float phase);

	uint8_t _partials;
	bool _bypass;
	bool _rising;
	float _frequency;
	float _amplitude;
	float* _a;
	float _phase_increment;
	float _phase_accumulator;
};

class AudioSynthSineSquareFast : public AudioSynthSinePartialFast
{
public:
	AudioSynthSineSquareFast(uint8_t p);
	~AudioSynthSineSquareFast();
	void duty_cycle(float dc);

protected:
        void _calculate(void);
	float _duty_cycle;
};

class AudioSynthSineTriangleFast : public AudioSynthSinePartialFast
{
public:
	AudioSynthSineTriangleFast(uint8_t p);
	~AudioSynthSineTriangleFast();

protected:
        void _calculate(void);
};

class AudioSynthSineSawtoothFast : public AudioSynthSinePartialFast
{
public:
	AudioSynthSineSawtoothFast(uint8_t p);
	AudioSynthSineSawtoothFast(uint8_t p, bool r);
	~AudioSynthSineSawtoothFast();
	void rising(bool r);
	void falling(bool r);

protected:
        void _calculate(void);
	float _internal_update_function(float phase);
	float _rising;
};

