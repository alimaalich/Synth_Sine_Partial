/*
 * For recording audio in Linux via USB:
 * AUDIO_DEVICE=$(pacmd list-sources | egrep '^\s+name: .*alsa_input' | grep Teensy | cut -d"<" -f2 | cut -d ">" -f1)
 * parecord --channels=1 -d "${AUDIO_DEVICE}" test.wav
 */

#include <Arduino.h>
#include <Audio.h>
#include "synth_sine_partial.h"
#include "synth_sine_partial_fast.h"

#define TEST_FREQUENCY 440.0
#define USE_MAX_PARTIALS 10

AudioSynthSineSquare sine_waveform_square(1);
AudioSynthSineSquareFast sine_waveform_square_fast(1);
AudioSynthSineTriangle sine_waveform_triangle(1);
AudioSynthSineTriangleFast sine_waveform_triangle_fast(1);
AudioSynthSineSawtooth sine_waveform_sawtooth(1, true);
AudioSynthSineSawtoothFast sine_waveform_sawtooth_fast(1, true);
AudioMixer4 mixer_r;
AudioMixer4 mixer_l;
AudioOutputUSB usb;
//AudioOutputI2S i2s;
AudioOutputPT8211 i2s;

AudioConnection patchCord0(sine_waveform_square, 0, mixer_r, 0);
AudioConnection patchCord1(sine_waveform_triangle, 0, mixer_r, 1);
AudioConnection patchCord2(sine_waveform_sawtooth, 0, mixer_r, 2);
AudioConnection patchCord3(sine_waveform_square_fast, 0, mixer_l, 0);
AudioConnection patchCord4(sine_waveform_triangle_fast, 0, mixer_l, 1);
AudioConnection patchCord5(sine_waveform_sawtooth_fast, 0, mixer_l, 2);
AudioConnection patchCord6(mixer_r, 0, usb, 0);
AudioConnection patchCord7(mixer_l, 0, usb, 1);
AudioConnection patchCord8(mixer_r, 0, i2s, 0);
AudioConnection patchCord9(mixer_l, 0, i2s, 1);

void setup() {
  AudioMemory(16);
  Serial.begin(115200);

  Serial.printf("<SETUP START>\n");

  sine_waveform_square.amplitude(1.0);
  sine_waveform_square_fast.amplitude(1.0);
  sine_waveform_triangle.amplitude(1.0);
  sine_waveform_triangle_fast.amplitude(1.0);
  sine_waveform_sawtooth.amplitude(1.0);
  sine_waveform_sawtooth_fast.amplitude(1.0);

  mixer_r.gain(0, 1.0);
  mixer_r.gain(1, 1.0);
  mixer_r.gain(2, 1.0);
  mixer_r.gain(3, 0.0);
  mixer_l.gain(0, 1.0);
  mixer_l.gain(1, 1.0);
  mixer_l.gain(2, 1.0);
  mixer_l.gain(3, 0.0);

  AudioProcessorUsageMaxReset();
  AudioMemoryUsageMaxReset();

  Serial.printf("<SETUP END>\n");
  delay(1000);
}

void loop() {
  uint8_t partials, i;

  Serial.printf("<LOOP START>\n");

  for (i = 0; i < 4; i++) {
    note(i, 1, TEST_FREQUENCY);
    for (partials = 1; partials <= USE_MAX_PARTIALS; partials++) {
      Serial.printf("PARTIALS=%d ", partials);
      note(i, partials, TEST_FREQUENCY);
      delay(1000);
      Serial.printf("CPU = %f%%, CPU_MAX = %f%%\n", AudioProcessorUsage(), AudioProcessorUsageMax());
    }
    note(i, partials, 0.0);
  }
  Serial.printf("<LOOP END>\n");
}

void note(uint8_t w, uint8_t p, float f) {
  if (f != 0.0)
    Serial.printf("Note-On ");
  else
    Serial.printf("Note-Off ");

  switch (w) {
    case 0:
      Serial.printf("SQUARE 20%% ");
      sine_waveform_square.partials(p);
      sine_waveform_square.duty_cycle(0.4);
      sine_waveform_square.frequency(f);
      sine_waveform_square_fast.partials(p);
      sine_waveform_square_fast.duty_cycle(0.4);
      sine_waveform_square_fast.frequency(f);
      break;
    case 1:
      Serial.printf("SQUARE 50%% ");
      sine_waveform_square.partials(p);
      sine_waveform_square.duty_cycle(1.0);
      sine_waveform_square.frequency(f);
      sine_waveform_square_fast.partials(p);
      sine_waveform_square_fast.duty_cycle(1.0);
      sine_waveform_square_fast.frequency(f);
      break;
    case 2:
      Serial.printf("TRIANGLE ");
      sine_waveform_triangle.partials(p);
      sine_waveform_triangle.frequency(f);
      sine_waveform_triangle_fast.partials(p);
      sine_waveform_triangle_fast.frequency(f);
      break;
    case 3:
      Serial.printf("SAWTOOTH ");
      sine_waveform_sawtooth.partials(p);
      sine_waveform_sawtooth.frequency(f);
      sine_waveform_sawtooth_fast.partials(p);
      sine_waveform_sawtooth_fast.frequency(f);
      break;
  }
  Serial.printf("\n");
}
